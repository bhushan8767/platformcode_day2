class Compute 
{
    public long[] minAnd2ndMin(long a[], long n)  
    {
        long min1=Long.MAX_VALUE;
        long min2=Long.MAX_VALUE;
        int m=0;
        for(int i=0;i<a.length;i++){
                if(min1>a[i]){
                    min1=a[i];
                    m=i;
                }
        }
        for(int i=0;i<a.length;i++){
            if(a[m]!=a[i]){
                if(min2>a[i] ){
                    min2=a[i];
                }
            }
        }
        long[] arr=new long[]{-1,-1};
        if(min1 != Long.MAX_VALUE && min2 != Long.MAX_VALUE){
            arr[0]=min1;
            arr[1]=min2;
            return arr;
        }
        else{
            return arr;
        }
    }
}
